CREATE SCHEMA LABS_FATEC_SJC;

USE LABS_FATEC_SJC;

CREATE TABLE USR_USER (
  USR_ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  USR_NAME VARCHAR(50) NOT NULL,
  USR_MAIL VARCHAR(50) NOT NULL,
  USR_PASSWORD VARCHAR(50) NOT NULL,
  PRIMARY KEY (USR_ID),
  UNIQUE KEY UNI_USER_NAME (USR_NAME)
);

CREATE TABLE AUT_AUTHORIZATION (
  AUT_ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  AUT_NAME VARCHAR(20) NOT NULL,
  PRIMARY KEY (AUT_ID),
  UNIQUE KEY UNI_AUT_NAME (AUT_NAME)
);

CREATE TABLE UAU_USER_AUTHORIZATION (
  USR_ID BIGINT UNSIGNED NOT NULL,
  AUT_ID BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (USR_ID, AUT_ID),
  FOREIGN KEY AUT_USER_FK (USR_ID) REFERENCES USR_USER (USR_ID) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY AUT_AUTHORIZATION_FK (AUT_ID) REFERENCES AUT_AUTHORIZATION (AUT_ID) ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE LAB_LABORATORY (
  LAB_ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  LAB_CODE VARCHAR(3) NOT NULL,
  LAB_NUMBER_PCS INT NOT NULL,
  PRIMARY KEY (LAB_ID),
  UNIQUE KEY UNI_LAB_CODE (LAB_CODE)
);
CREATE TABLE RL_RESERVE_LABS (
  RL_ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  LAB_ID BIGINT UNSIGNED NOT NULL,
  USR_ID BIGINT UNSIGNED NOT NULL,
  RL_PERIOD INT NOT NULL,
  RL_DATE DATE NOT NULL,
  RL_START INT NOT NULL,
  RL_FINISH INT NOT NULL,
  FOREIGN KEY SL_LAB_FK (LAB_ID) REFERENCES LAB_LABORATORY (LAB_ID),
  FOREIGN KEY SL_USR_FK (USR_ID) REFERENCES USR_USER (USR_ID),
  PRIMARY KEY (RL_ID)
);
  
CREATE TABLE SOFT_SOFTWARE (
  SOFT_ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  SOFT_NAME VARCHAR(25) NOT NULL,
  SOFT_VERSION VARCHAR(10) NOT NULL,
  PRIMARY KEY (SOFT_ID),
  UNIQUE KEY UNI_SOFT_NAME (SOFT_NAME)
);

CREATE TABLE SL_SOFT_LABORATORY (
  SL_ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  LAB_ID BIGINT UNSIGNED NOT NULL,
  SOFT_ID BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (SL_ID),
  UNIQUE KEY UNI_SOFT_LABORATORY (LAB_ID,SOFT_ID),
  FOREIGN KEY SL_LAB_FK (LAB_ID) REFERENCES LAB_LABORATORY (LAB_ID),
  FOREIGN KEY SL_SOFT_FK (SOFT_ID) REFERENCES SOFT_SOFTWARE (SOFT_ID)
);